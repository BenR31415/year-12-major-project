<?php
$user = $_SERVER['REMOTE_USER'];
//echo $user;
$temperature = shell_exec('/home/odroid/bin/check_temp.sh');
if ($temperature < 50) {
    $temperatureid = "greentext";
}
else if (temperature > 70){
    $temperatureid = "redtext";
}
else {
    $temperatureid = "orangetext";
}

//echo $temperature;
$powerfeedastatus = shell_exec('/home/odroid/bin/powerfeeda.sh');
if ($powerfeedastatus == 1) {
    $feedaid = "greentext";
    $feedacontent = "Active";
    //echo "Feed A loop1";
}
else {
    $feedaid = "redtext";
    $feedacontent = "Inactive";
    //echo "Feed A loop2";
}
//echo $powerfeedastatus;
$powerfeedbstatus = shell_exec('/home/odroid/bin/powerfeedb.sh');
if ($powerfeedbstatus == 1) {
    $feedbid = "greentext";
    $feedbcontent = "Active";
}
else {
    $feedbid = "redtext";
    $feedbcontent = "Inactive";
}
//echo $powerfeedbstatus;

$switchastatus = shell_exec('/home/odroid/bin/web_switchacheck.sh');
//echo "<br>" . $switchastatus;
if ($switchastatus == 0){
    $switcha2id = "redbutton";
    $switcha3id = "greenbutton";
    //echo "<br>Switch A status = 0<br>";
}
else {
    $switcha2id = "greenbutton";
    $switcha3id = "redbutton";
    //echo "<br>Switch A status != 0<br>";
}

$switchbstatus = shell_exec('/home/odroid/bin/web_switchbcheck.sh');
//echo "<br>" . $switchbstatus;
if ($switchbstatus == 0){
    $switchb2id = "redbutton";
    $switchb3id = "greenbutton";
    //echo "<br>Switch B status = 0<br>";
}
else {
    $switchb2id = "greenbutton";
    $switchb3id = "redbutton";
    //echo "<br>Switch B status != 0<br>";
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(isset($_POST['a2'])){
        //Run Path a2 Bash script
        $message = shell_exec("/home/odroid/bin/patha2.sh $user");
	//echo "Switch A2";
    }
    else if(isset($_POST['a3'])){
        //Run Path a3 Bash script
        $message = shell_exec("/home/odroid/bin/patha3.sh $user");
	//echo "Switch A3";
    }
    else if(isset($_POST['b2'])){
        //Run Path b2 Bash script
        $message = shell_exec("/home/odroid/bin/pathb2.sh $user");
	//echo "Switch B2";
    }
    else if(isset($_POST['b3'])){
        //Run Path b3 Bash script
        $message = shell_exec("/home/odroid/bin/pathb3.sh $user");
	//echo "Switch B3";
    }
    //echo $message;
    date_default_timezone_set("GMT-5");
    echo date_default_timezone_get();
 
}
//echo date("D") . ', ' . date("d/m/y") . ', ' . date("H");
?>

<html>
<head>
    <title>Page 1!</title>
    <link rel="stylesheet" href="style_1.css">
    <meta http-equiv="refresh" content="10;URL=<?php $_SERVER['PHP_SELF']; ?>'">
</head>
<body>
    <div id="headerwrapper">
        <div id="logocontainer">
            <img src="images/logo.png" alt="/images/banner.png" width="100%" height="100%">
        </div>
        <div id="criticalinformation">
            <?php echo date("D") . ', ' . date("d/m/y") . ', ' . date("H:i:s") . " | User: " . $user . ' | <span id="' . $temperatureid . '">' . $temperature . 'Degrees C</span>' ?></span>
        </div>
    </div>
    <form action="" method="post">
        <div id="switchcontrolcontainer">
            <div class="individualswitchcontrol">
                <p class="boxtitle"><b>Switch A Control</b></p>
                <div class="switchbuttoncontainer">
		    <button type="submit" class="<?php echo $switcha2id; ?> switchbutton" id="a2" name="a2"><b>Fiber Path A1 to A2</b></button>
                </div>
                <br><br>
                <div class="switchbuttoncontainer">
                    <button type="submit" class="<?php echo $switcha3id; ?> switchbutton id="a3" name="a3""><b>Fiber Path A1 to A3</b></button>
                </div>
            </div>
            <div class="individualswitchcontrol">
                <p class="boxtitle"><b>Switch B Control</b></p>
                <div class="switchbuttoncontainer">
                    <button type="submit" class="<?php echo $switchb2id; ?> switchbutton" id="b2" name="b2"><b>Fiber Path B1 to B2</b></button>
                </div>
                <br><br>
                <div class="switchbuttoncontainer">
                    <button type="submit" class="<?php echo $switchb3id; ?> switchbutton" id="b3" name="b3"><b>Fiber Path B1 to B3</b></button>
                </div>
            </div>
        </div>
    </form>
    <div id="logs">
	<h3>Logs:</h3>
        <?php 
	    $reverselog =  array_reverse( explode("\n", file_get_contents('/var/log/optical.txt')));
            foreach($reverselog as $key=>$value) {
	    	echo '<p class="logtext">' . $reverselog[$key] . "</p>";
	    }
	?>
    </div>
    <div id="powersupplycontainer">
        <b>Power Supplies:</b><br>
        Feed A:<br><span id="<?php echo $feedaid; ?>"><b><?php echo $feedacontent; ?></b></span><br>
        Feed B:<br><span id="<?php echo $feedbid; ?>"><b><?php echo $feedbcontent; ?></b></span>

    </div>
</body>
</html>
