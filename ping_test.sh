#!/bin/bash
target="192.168.1.212"
# target="10.10.10.12"
# target="192.168.1.1"
count=10
while true ; do
returned=`ping -c $count $target 2>/tmp/ping_err.txt | grep received | sed -e 's/^.*transmitted, //' -e 's/ received.*//'`
echo $returned
if [ -z "$returned" ] ; then
echo "Null returned"
/home/odroid/bin/togglea.sh auto_null
sleep $count
else if [ "$returned" -lt 1 ] ; then
echo "None returned"
/home/odroid/bin/togglea.sh auto_null
fi
fi
done
