#!/bin/bash


psu1=1
psu2=2
led1=3
led2=4
led3=5
led4=6
switch2=7
switch3=11
switch5=21
switch6=22
switchstatuspin1=23
switchstatuspin2=24

assignpins () {
    gpio mode $psu1 in
    gpio mode $psu2 in
    gpio mode $led1 out
    gpio mode $led2 out
    gpio mode $led3 out
    gpio mode $led4 out
    gpio mode $switch2 out
    gpio mode $switch3 out
    gpio mode $switch5 out
    gpio mode $switch6 out   
    gpio mode $switchstatuspin1 in
    gpio mode $switchstatuspin2 in
}

assignpins
