#!/bin/bash
source /home/odroid/bin/pinmap.sh
source /home/odroid/bin/switch_check.sh

user=$1
if [$user == ""]
then
    user=$USER
fi
date=`date`

if [ $switch1path == 0 ]
then
    echo "Fibre has been toggled from path 1 -> 3 to 1 -> 2"
    gpio write $switch2 1
    gpio write $switch3 0
    gpio write $led1 1
    gpio write $led2 0
else
    echo "Fibre has been toggled from path 1 -> 2 to 1 -> 3"
    gpio write $switch3 1
    gpio write $switch2 0
    gpio write $led2 1
    gpio write $led1 0
fi

# echo "Switch3 = $switch3"
echo $switchstatuspin1
echo "Date: $date User: $user Ran: $0">>/var/log/optical.txt
