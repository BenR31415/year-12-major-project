# Year 12 Major Project

This is a retroactive upload of my major project in Year 12, completed in 2018. This project involved a physical fibre-optic switch incorporating a microcontroller running a web interface to remotely change fibre paths for remote network management.

This also includes code to have an automatic failover in the case of dropped packets.

[An informal video demonstration of the project can be found here, showing the automatic failover functionality.](https://youtu.be/6rkd-2pRFro)

The file "Major Project Final Presentation.pdf" contains photos of the project, and parts of the design broken down into smaller pieces.