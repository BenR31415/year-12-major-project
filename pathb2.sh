#!/bin/bash
source /home/odroid/bin/pinmap.sh
source /home/odroid/bin/switch_check.sh

user=$1
if [$user == ""]
then
    user=$USER
fi
date=`date`

if [ $switch2path == 0 ]
then
    echo "Fibre has been switched from path 4 -> 6 to 4 -> 5"
else
    echo "Fibre has is already at path 4 -> 5"
fi

# echo "Switch3 = $switch3"
gpio write $switch5 1
gpio write $switch6 0
gpio write $led3 1
gpio write $led4 0
echo "Date: $date User: $user Ran: $0">>/var/log/optical.txt
