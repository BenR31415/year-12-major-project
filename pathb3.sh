#!/bin/bash
source /home/odroid/bin/pinmap.sh
source /home/odroid/bin/switch_check.sh

user=$1
if [$user == ""]
then
    user=$USER
fi
date=`date`

if [ $switch2path == 1 ]
then
    echo "Fibre has been switched from path 4 -> 5 to 4 -> 6"
else
    echo "Fibre has is already at path 4 -> 6"
fi

# echo "Switch3 = $switch3"
gpio write $switch5 0
gpio write $switch6 1
gpio write $led3 0
gpio write $led4 1
echo "Date: $date User: $user Ran: $0">>/var/log/optical.txt
