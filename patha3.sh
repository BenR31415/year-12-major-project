#!/bin/bash
source /home/odroid/bin/pinmap.sh
source /home/odroid/bin/switch_check.sh

user=$1
if [$user == ""]
then
    user=$USER
fi
date=`date`

if [ $switch1path == 1 ]
then
    echo "Fibre has been switched from path 1 -> 2 to 1 -> 3"
else
    echo "Fibre has is already at path 1 -> 3"
fi

# echo "Switch3 = $switch3"
gpio write $switch2 0
gpio write $switch3 1
gpio write $led1 0
gpio write $led2 1
echo "Date: $date User: $user Ran: $0">>/var/log/optical.txt
