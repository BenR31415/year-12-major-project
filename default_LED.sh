#!/bin/bash
source pinmap.sh

changeLED "$ledon" "$ledoff" {
    gpio write $ledon 1;
    gpio write $ledoff 0;
}